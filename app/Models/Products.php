<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Products extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    protected $fillable = ['name', 'category_id'];

    /**
     * Get the category record associated with the product.
     */
    public function category()
    {
        return $this->hasOne('App\Model\Categories');
    }

    /**
     * Get the images records associated with the products.
     */
    public function images()
    {
        return $this->hasMany('App\Models\Images', 'product_id', 'id');
    }

}
