<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 7/15/2018
 * Time: 3:32 PM
 */

namespace App\Transformers;

use App\Models\Categories;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'products'
    ];

    public function transform(Categories $category)
    {
        return [
            'id' => $category->id,
            'name' => $category->name
        ];
    }

    /**
     * Include Products
     *
     * @param Categories $category
     * @return Collection
     */
    public function includeProducts(Categories $category)
    {
        $products = $category->products;

        if (!empty($products)) {
            return $this->collection($products, new ProductsTransformer());
        }
    }
}