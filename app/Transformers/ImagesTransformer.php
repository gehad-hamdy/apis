<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 7/15/2018
 * Time: 3:44 PM
 */

namespace App\Transformers;


use App\Models\Images;
use League\Fractal;

class ImagesTransformer extends Fractal\TransformerAbstract
{
    public function transform(Images $image)
    {
        return [
            'name' => $image->name
        ];
    }
}