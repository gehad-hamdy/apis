<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 7/15/2018
 * Time: 3:38 PM
 */

namespace App\Transformers;


use App\Models\Products;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;

class ProductsTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'images'
    ];

    public function transform(Products $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price
        ];
    }

    /**
     * Include Images
     * @param Products $product
     * @return Collection
     */
    public function includeImages(Products $product)
    {
        $images = $product->images;
        if (!empty($images)) {
            return $this->Collection($images, new ImagesTransformer());
        }
    }

}