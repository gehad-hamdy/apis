<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Get the products records associated with the category.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Products', 'category_id', 'id');
    }
}
