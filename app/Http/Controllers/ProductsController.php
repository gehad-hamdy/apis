<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $categories = Categories::all();

//        return $x = Categories::with('products')->where('id', 1);
//
//        foreach ($categories as $category) {
//            return $category->products;
//        }
        $categoriesTransformers = fractal($categories, new CategoryTransformer())->toArray();

        return response()->json($categoriesTransformers);
    }
}
